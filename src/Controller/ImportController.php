<?php

namespace App\Controller;

use App\Entity\Import;
use App\Form\ImportType;
use App\Repository\ImportRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ImportController extends AbstractController
{
    private $importRepository;

    public function __construct(ImportRepository $importRepository)
    {
        $this->importRepository = $importRepository;
    }

    /**
     * @Route("/", name="app_import_list", methods={"GET"})
     */
    public function list(): Response
    {
        $imports = $this->importRepository->findAll();

        return $this->render('import/list.html.twig', [
            'imports' => $imports,
        ]);
    }

    /**
     * @Route("/new", name="app_import_new", methods={"GET", "POST"})
     */
    public function new(Request $request): Response
    {
        $import = new Import();
        $form = $this->createForm(ImportType::class, $import);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->importRepository->add($import, true);

            return $this->redirectToRoute('app_import_list');
        }

        return $this->render('import/new.html.twig', [
            'import' => $import,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="app_import_show", methods={"GET"})
     */
    public function show(Import $import): Response
    {
        return $this->render('import/show.html.twig', [
            'import' => $import,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_import_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Import $import): Response
    {
        $form = $this->createForm(ImportType::class, $import);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->importRepository->add($import, true);

            return $this->redirectToRoute('app_import_list');
        }

        return $this->render('import/edit.html.twig', [
            'import' => $import,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="app_import_delete", methods={"POST"})
     */
    public function delete(Request $request, Import $import): Response
    {
        if ($this->isCsrfTokenValid('delete' . $import->getId(), $request->request->get('_token'))) {
            $this->importRepository->remove($import, true);
        }

        return $this->redirectToRoute('app_import_list');
    }
}
