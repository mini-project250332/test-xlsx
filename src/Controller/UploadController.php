<?php

namespace App\Controller;

use App\Form\UploadType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use App\Service\ExcelImporterService;
use App\Entity\Import;

/**
 * @Route("/upload")
 */
class UploadController extends AbstractController
{
    /**
     * @Route("/do_import", name="do_import")
     */
    public function Import(Request $request): Response
    {
        $form = $this->createForm(UploadType::class);
        $form->handleRequest($request);

        return $this->render('upload/import_file.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/upload", name="upload")
     */
    public function upload(Request $request, ExcelImporterService $excelImporterService)
    {
        $form = $this->createForm(UploadType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file = $form->get('file')->getData();

            if ($file instanceof UploadedFile) {
                if ($file->getClientOriginalExtension() !== 'xlsx') {
                    $this->addFlash('error', 'Le fichier doit être au format XLSX.');
                    return $this->redirectToRoute('do_import');
                } else {
                    $importedCount = $excelImporterService->import($file);

                    if ($importedCount > 0) {
                        $this->addFlash('success', 'Les données sont enregistrées.');
                        return $this->redirectToRoute('do_import');
                    } else {
                        $this->addFlash('error', 'Aucune donnée valide n\'a été importée.');
                        return $this->redirectToRoute('do_import');
                    }
                }
            }
        }
    }
}
