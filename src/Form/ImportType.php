<?php

namespace App\Form;

use App\Entity\Import;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Validator\Constraints\NotBlank;

class ImportType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('compte_affaire', null, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez fournir une valeur pour le champ Compte affaire',
                    ]),
                ],
            ])
            ->add('compte_evenement')
            ->add('compte_dernier_event')
            ->add('numero_fiche')
            ->add('libelle_civilite')
            ->add('propriete_vehicule')
            ->add('nom')
            ->add('prenom')
            ->add('num_nom_voie')
            ->add('complement_adresse')
            ->add('code_postal')
            ->add('ville')
            ->add('telephone_domicile')
            ->add('telephone_portable')
            ->add('telephone_job')
            ->add('email')
            ->add('date_mise_en_circulation', DateType::class, [
                'widget' => 'single_text',
                'input' => 'datetime',
            ])
            ->add('date_achat', DateType::class, [
                'widget' => 'single_text',
                'input' => 'datetime',
            ])
            ->add('date_dernier_event', DateType::class, [
                'widget' => 'single_text',
                'input' => 'datetime',
            ])
            ->add('libelle_marque')
            ->add('libelle_modele')
            ->add('version')
            ->add('vin')
            ->add('immatriculation')
            ->add('type_prospect')
            ->add('kilometrage')
            ->add('libelle_energie')
            ->add('vendeur_vn')
            ->add('vendeur_vo')
            ->add('commentaire_facturation')
            ->add('type_vn_vo')
            ->add('num_dossier_vn_vo')
            ->add('intermediaire_vente')
            ->add('date_evenement_veh', DateType::class, [
                'widget' => 'single_text',
                'input' => 'datetime',
            ])
            ->add('originine_evenement');
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Import::class,
        ]);
    }
}
