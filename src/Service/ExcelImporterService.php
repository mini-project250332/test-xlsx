<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpKernel\KernelInterface;
use App\Entity\Import;

class ExcelImporterService
{
    private $entityManager;
    private $kernel;

    public function __construct(EntityManagerInterface $entityManager, KernelInterface $kernel)
    {
        $this->entityManager = $entityManager;
        $this->kernel = $kernel;
    }

    public function import(UploadedFile $file): int
    {
        $uploadsDirectory = $this->kernel->getProjectDir() . '/public/uploads';
        $filename = md5(uniqid()) . '.' . $file->getClientOriginalExtension();
        $file->move($uploadsDirectory, $filename);

        $spreadsheet = IOFactory::load($uploadsDirectory . '/' . $filename);
        $worksheet = $spreadsheet->getActiveSheet();
        $data = $worksheet->toArray();

        $importedCount = 0;

        if (!empty($data)) {
            foreach ($data as $key => $row) {
                if ($key === 0 || count($row) !== 35) {
                    continue;
                }

                $import = new Import();
                $this->fillImportFromRow($import, $row);

                $this->entityManager->persist($import);
                $importedCount++;
            }

            $this->entityManager->flush();
        }

        return $importedCount;
    }

    private function fillImportFromRow(Import $import, array $row): void
    {
        $createDateTimeOrNull = function ($value) {
            return !empty($value) ? new \DateTime($value) : null;
        };

        $import->setCompteAffaire($row[0]);
        $import->setCompteEvenement($row[1]);
        $import->setCompteDernierEvent($row[2]);
        $import->setNumeroFiche($row[3]);
        $import->setLibelleCivilite($row[4]);
        $import->setProprieteVehicule($row[5]);
        $import->setNom($row[6]);
        $import->setPrenom($row[7]);
        $import->setNumNomVoie($row[8]);
        $import->setComplementAdresse($row[9]);
        $import->setCodePostal($row[10]);
        $import->setVille($row[11]);
        $import->setTelephoneDomicile($row[12]);
        $import->setTelephonePortable($row[13]);
        $import->setTelephoneJob($row[14]);
        $import->setEmail($row[15]);

        $import->setDateMiseEnCirculation($createDateTimeOrNull($row[16]));
        $import->setDateAchat($createDateTimeOrNull($row[17]));
        $import->setDateDernierEvent($createDateTimeOrNull($row[18]));

        $import->setLibelleMarque($row[19]);
        $import->setLibelleModele($row[20]);
        $import->setVersion($row[21]);
        $import->setVin($row[22]);
        $import->setImmatriculation($row[23]);
        $import->setTypeProspect($row[24]);
        $import->setKilometrage($row[25]);
        $import->setLibelleEnergie($row[26]);
        $import->setVendeurVn($row[27]);
        $import->setVendeurVo($row[28]);
        $import->setCommentaireFacturation($row[29]);
        $import->setTypeVnVo($row[30]);
        $import->setNumDossierVnVo($row[31]);
        $import->setIntermediaireVente($row[32]);

        $import->setDateDernierEvent($createDateTimeOrNull($row[33]));

        $import->setOriginineEvenement($row[34]);
    }
}
