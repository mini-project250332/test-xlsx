<?php

namespace App\Entity;

use App\Repository\ImportRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ImportRepository::class)
 */
class Import
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=60, nullable=false)
     */
    private $compte_affaire;

    /**
     * @ORM\Column(type="string", length=60, nullable=true)
     */
    private $compte_evenement;

    /**
     * @ORM\Column(type="string", length=70, nullable=true)
     */
    private $compte_dernier_event;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $numero_fiche;

    /**
     * @ORM\Column(type="string", length=9, nullable=true)
     */
    private $libelle_civilite;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $propriete_vehicule;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=70, nullable=true)
     */
    private $prenom;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $num_nom_voie;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $complement_adresse;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $code_postal;

    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     */
    private $ville;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $telephone_domicile;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $telephone_portable;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $telephone_job;

    /**
     * @ORM\Column(type="string", length=60, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $date_mise_en_circulation;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $date_achat;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $date_dernier_event;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $libelle_marque;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $libelle_modele;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $version;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $vin;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $immatriculation;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $type_prospect;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $kilometrage;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $libelle_energie;

    /**
     * @ORM\Column(type="string", length=60, nullable=true)
     */
    private $vendeur_vn;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $vendeur_vo;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $commentaire_facturation;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $type_vn_vo;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $num_dossier_vn_vo;

    /**
     * @ORM\Column(type="string", length=70, nullable=true)
     */
    private $intermediaire_vente;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $date_evenement_veh;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $originine_evenement;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCompteAffaire(): ?string
    {
        return $this->compte_affaire;
    }

    public function setCompteAffaire(string $compte_affaire): self
    {
        $this->compte_affaire = $compte_affaire;

        return $this;
    }

    public function getCompteEvenement(): ?string
    {
        return $this->compte_evenement;
    }

    public function setCompteEvenement(?string $compte_evenement): self
    {
        $this->compte_evenement = $compte_evenement;

        return $this;
    }

    public function getCompteDernierEvent(): ?string
    {
        return $this->compte_dernier_event;
    }

    public function setCompteDernierEvent(?string $compte_dernier_event): self
    {
        $this->compte_dernier_event = $compte_dernier_event;

        return $this;
    }

    public function getNumeroFiche(): ?int
    {
        return $this->numero_fiche;
    }

    public function setNumeroFiche(?int $numero_fiche): self
    {
        $this->numero_fiche = $numero_fiche;

        return $this;
    }

    public function getLibelleCivilite(): ?string
    {
        return $this->libelle_civilite;
    }

    public function setLibelleCivilite(?string $libelle_civilite): self
    {
        $this->libelle_civilite = $libelle_civilite;

        return $this;
    }

    public function getProprieteVehicule(): ?string
    {
        return $this->propriete_vehicule;
    }

    public function setProprieteVehicule(?string $propriete_vehicule): self
    {
        $this->propriete_vehicule = $propriete_vehicule;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(?string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getNumNomVoie(): ?string
    {
        return $this->num_nom_voie;
    }

    public function setNumNomVoie(?string $num_nom_voie): self
    {
        $this->num_nom_voie = $num_nom_voie;

        return $this;
    }

    public function getComplementAdresse(): ?string
    {
        return $this->complement_adresse;
    }

    public function setComplementAdresse(?string $complement_adresse): self
    {
        $this->complement_adresse = $complement_adresse;

        return $this;
    }

    public function getCodePostal(): ?string
    {
        return $this->code_postal;
    }

    public function setCodePostal(?string $code_postal): self
    {
        $this->code_postal = $code_postal;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(?string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getTelephoneDomicile(): ?string
    {
        return $this->telephone_domicile;
    }

    public function setTelephoneDomicile(?string $telephone_domicile): self
    {
        $this->telephone_domicile = $telephone_domicile;

        return $this;
    }

    public function getTelephonePortable(): ?string
    {
        return $this->telephone_portable;
    }

    public function setTelephonePortable(?string $telephone_portable): self
    {
        $this->telephone_portable = $telephone_portable;

        return $this;
    }

    public function getTelephoneJob(): ?string
    {
        return $this->telephone_job;
    }

    public function setTelephoneJob(?string $telephone_job): self
    {
        $this->telephone_job = $telephone_job;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getDateMiseEnCirculation(): ?\DateTimeInterface
    {
        return $this->date_mise_en_circulation;
    }

    public function setDateMiseEnCirculation(?\DateTimeInterface $date_mise_en_circulation): self
    {
        $this->date_mise_en_circulation = $date_mise_en_circulation;

        return $this;
    }

    public function getDateAchat(): ?\DateTimeInterface
    {
        return $this->date_achat;
    }

    public function setDateAchat(?\DateTimeInterface $date_achat): self
    {
        $this->date_achat = $date_achat;

        return $this;
    }

    public function getDateDernierEvent(): ?\DateTimeInterface
    {
        return $this->date_dernier_event;
    }

    public function setDateDernierEvent(?\DateTimeInterface $date_dernier_event): self
    {
        $this->date_dernier_event = $date_dernier_event;

        return $this;
    }

    public function getLibelleMarque(): ?string
    {
        return $this->libelle_marque;
    }

    public function setLibelleMarque(?string $libelle_marque): self
    {
        $this->libelle_marque = $libelle_marque;

        return $this;
    }

    public function getLibelleModele(): ?string
    {
        return $this->libelle_modele;
    }

    public function setLibelleModele(?string $libelle_modele): self
    {
        $this->libelle_modele = $libelle_modele;

        return $this;
    }

    public function getVersion(): ?string
    {
        return $this->version;
    }

    public function setVersion(?string $version): self
    {
        $this->version = $version;

        return $this;
    }

    public function getVin(): ?string
    {
        return $this->vin;
    }

    public function setVin(?string $vin): self
    {
        $this->vin = $vin;

        return $this;
    }

    public function getImmatriculation(): ?string
    {
        return $this->immatriculation;
    }

    public function setImmatriculation(?string $immatriculation): self
    {
        $this->immatriculation = $immatriculation;

        return $this;
    }

    public function getTypeProspect(): ?string
    {
        return $this->type_prospect;
    }

    public function setTypeProspect(?string $type_prospect): self
    {
        $this->type_prospect = $type_prospect;

        return $this;
    }

    public function getKilometrage(): ?string
    {
        return $this->kilometrage;
    }

    public function setKilometrage(?string $kilometrage): self
    {
        $this->kilometrage = $kilometrage;

        return $this;
    }

    public function getLibelleEnergie(): ?string
    {
        return $this->libelle_energie;
    }

    public function setLibelleEnergie(?string $libelle_energie): self
    {
        $this->libelle_energie = $libelle_energie;

        return $this;
    }

    public function getVendeurVn(): ?string
    {
        return $this->vendeur_vn;
    }

    public function setVendeurVn(?string $vendeur_vn): self
    {
        $this->vendeur_vn = $vendeur_vn;

        return $this;
    }

    public function getVendeurVo(): ?string
    {
        return $this->vendeur_vo;
    }

    public function setVendeurVo(?string $vendeur_vo): self
    {
        $this->vendeur_vo = $vendeur_vo;

        return $this;
    }

    public function getCommentaireFacturation(): ?string
    {
        return $this->commentaire_facturation;
    }

    public function setCommentaireFacturation(?string $commentaire_facturation): self
    {
        $this->commentaire_facturation = $commentaire_facturation;

        return $this;
    }

    public function getTypeVnVo(): ?string
    {
        return $this->type_vn_vo;
    }

    public function setTypeVnVo(?string $type_vn_vo): self
    {
        $this->type_vn_vo = $type_vn_vo;

        return $this;
    }

    public function getNumDossierVnVo(): ?string
    {
        return $this->num_dossier_vn_vo;
    }

    public function setNumDossierVnVo(?string $num_dossier_vn_vo): self
    {
        $this->num_dossier_vn_vo = $num_dossier_vn_vo;

        return $this;
    }

    public function getIntermediaireVente(): ?string
    {
        return $this->intermediaire_vente;
    }

    public function setIntermediaireVente(?string $intermediaire_vente): self
    {
        $this->intermediaire_vente = $intermediaire_vente;

        return $this;
    }

    public function getDateEvenementVeh(): ?\DateTimeInterface
    {
        return $this->date_evenement_veh;
    }

    public function setDateEvenementVeh(?\DateTimeInterface $date_evenement_veh): self
    {
        $this->date_evenement_veh = $date_evenement_veh;

        return $this;
    }

    public function getOriginineEvenement(): ?string
    {
        return $this->originine_evenement;
    }

    public function setOriginineEvenement(?string $originine_evenement): self
    {
        $this->originine_evenement = $originine_evenement;

        return $this;
    }
}
