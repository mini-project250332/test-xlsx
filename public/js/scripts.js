$(document).ready(function () {
    $('#product-table').DataTable({
        "pageLength": 10, 
        "order": [] 
    });
});

function masquerMessageFlash(elementId, delai) {
    const element = document.getElementById(elementId);
    if (element) {
        setTimeout(function () {
            element.style.display = 'none';
        }, delai);
    }
}

masquerMessageFlash('error-message', 5000);
masquerMessageFlash('success-message', 5000);
